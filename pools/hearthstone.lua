local utils = require('pity/utils');

local pool = {};

local picker_hearthstone = function(pity,key,set,userstate,pity_shown)
  if(set ~= pool.hearthstone.all) then
    -- we are picking from good set
    return set[utils:to_int(math.mt_random(1,#set))]; 
  end

  -- https://hearthstone.gamepedia.com/Card_pack_statistics
  -- https://www.reddit.com/r/hearthstone/comments/69dexs/china_announces_hearthstone_card_pack_rarity_odds/
  --
  -- My goal wasn't 100% accuracy.  It isn't possible as the details of drop rate, or even how frequent common card 1 vs 2 show up are
  -- changable by blizzard.  The only industry where math like this is audited is Casino Slot machines.
  --
  -- realistically you wouldn't be using string comparisons, you'd convert to bit flags for performance
  -- but this is illustrative

  local odds = pool.hearthstone.odds; -- odds, which when pity is applied, approximate observed probabilities (this is a GUESS, one based on observations of players)
  local card_type = math.mt_random();
  local pick1 = nil;
  if(card_type < odds[1]) then
    pick1=set[1];
  elseif(card_type < odds[2]) then
    pick1=set[2];
  elseif(card_type < odds[3]) then
    pick1=set[3];
  else
    pick1=set[4];
  end

  if(pool.hearthstone.x < 1) then
    -- disable any influence of pools
    return pick1;
  end

  local pick2,shown_pity2 = utils:passert(pity:pick(string.format("%s_rare",key)));
  local pick3,shown_pity3 = utils:passert(pity:pick(string.format("%s_epic",key)));

  if(pick1 == "legendary") then
    userstate[string.format("%s_rare",key)] = 0;
    userstate[string.format("%s_epic",key)] = 0;
  elseif(pick1 == "epic") then
    userstate[string.format("%s_rare",key)] = 0;
    userstate[string.format("%s_epic",key)] = 0;
  elseif(pick1 == "rare") then
    userstate[string.format("%s_rare",key)] = 0;
  end

  if(pick1 == "common") then -- upgrade picks
    if(pick3 ~= "common") then
      userstate[string.format("%s_rare",key)] = 0;
      return pick3,{pity_epic_count=shown_pity3 and 1 or 0}; -- pity epic
    end
    if(pick2 ~= "common") then
      return pick2,{pity_rare_count=shown_pity2 and 1 or 0}; -- pity rare
    end
  end
  return pick1; -- default probabilities
end

local picker_first = function(pity,key,set,userstate,pity_shown)
  return set[1];
end


-- going to have a set of pools, the only requirement is one pool must be named the same as the key (aka the filename)
pool = {
  hearthstone={
    odds={0.830,0.95,0.99},
    all={ "common", "rare", "epic", "legendary" },
    bad={"common","rare","epic"},
    picker=picker_hearthstone,
    x=5*40-1, -- 1 in 40 packs
    additional_registration = { "hearthstone_rare", "hearthstone_epic" }, -- share the same userstate
    description=function()
      return [[--
This hearthstone stimulator uses 3 pity pools.  1 Legendary per 40 packs.  1 epic per 5 packs.  1 rare or better per pack.  This means that a card may be promoted (a rare pity may become a legendary pity, as an example).  In addition to pity pools there is also the pick probabilities (the baseline likeliness of a rare, epic, or legendary).  While researching, this appears to be hidden.  We have some observed probabilities.  I've attempted to pick probabilities which with pities give close to the observed rate.  The --x parameter only influences the legendary pity, the rest can be adjusted in pity/pool/hearthstone.lua (defaulting to 200 cards per pitied legendary).
--]];
    end,
  },
  hearthstone_rare={
    all={"common","rare"},
    bad={"common"},
    picker=picker_first,
    x=1*5-1 -- a rare every pack
  },
  hearthstone_epic={
    all={"common","epic"},
    bad={"common"},
    picker=picker_first,
    x=5*10-1 -- an epic every 10 packs
  },
  is_set = true, -- a set of pools
};

return pool;
