
local utils = require('pity/utils');
local pool;
local non_fuels = 100;

local picker = function(pity,key,set,userstate,pity_shown)
  if(set ~= pool.all) then
    return set[utils:to_int(math.mt_random(1,#set))];
  end
  local r = math.mt_random();
  for i,v in ipairs(pool.odds) do
    if(r < v) then return set[i]; end 
  end
  return set[#set];
end

pool = {
  all = utils:a_concat({"fuel"},utils:a_create("not-fuel",non_fuels,true),true),
  bad = {"not-fuel"},
  x = 2,
  odds = utils:a_concat({0.05},utils:a_create(function(tbl,i)
    local p =  ((1.0-0.05)/(non_fuels*1.0+1));
    p = p*(i*1.0+1)+0.05;
    return p; end,non_fuels,true),true ),
  picker = picker,
};

return pool;
