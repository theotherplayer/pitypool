
local pool = {};
local sets = require('pity/sets');
local utils = require('pity/utils');

-- 1% chance at a legendary, but you will see one every 20 picks
-- common items have a 20% chance of being promoted, and every 4th common will be promoted

local subpools = {"promoted"};
-- uncomment the following line if you wish to have 3 subpools (i.e. pool 1 converts "bad" at 5%), if that fails pool 2 converts the remaining bad at 5%, etc
-- this is how you scale additional loot drops with pity pools
subpools = {"promoted","promoted2","promoted3"}; -- note, each further subpool has "less" and "less" to draw from, since the prior one takes up some of its slots

local picker = function(pity,key,set,userstate,pity_shown)
  if(pity_shown) then
    local pick = set[utils:to_int(math.mt_random(1,#set))];
    return pick;
  end
  local originalkey = key;
  local promoted_key = string.gsub(key,"tiered_loot","promoted");
  key = string.gsub(key,"pools/",""); -- the key is passed form the commandline, mapping it to the pool datastructure, if you move the paths around this will break

  local pick;
  local r = math.mt_random();
  for i,v in ipairs(pool[key].odds) do
    if(r < v) then pick = set[i]; goto DONE; end
  end
  if(pick == nil) then pick = set[1]; end -- you did your odds wrong, fall back to common
  ::DONE::
  local annotation = nil;

  if(sets:contains(pool.tiered_loot.bad,pick)) then -- the pick is in the bad set

    for i=1,#subpools do
      local subpool = subpools[i];
      local promoted_key = string.gsub(originalkey,"tiered_loot",subpool);
 
      local pick2,pity = utils:passert(pity:pick(promoted_key));
      if(not sets:contains(pool[subpool].bad,pick2)) then -- the pick is NOT in the original bad set, i.e. its the new class of loot (ammo)
        pick = pick2; -- promoted
        local tmp = string.format("pity_%s",pick);
        annotation = {};
        annotation[tmp] = pity and 1 or 0;
        goto DONE2;
      end
    end
  end
  ::DONE2::
  if(annotation ~= nil) then
    return pick,annotation;
  end
  return pick;
end

local picker_promoted = function(pity,key,set,userstate,pity_shown)
  if(pity_shown) then
    -- set is "pool.good"
    local r = math.mt_random(1,#set);
    local pick = set[utils:to_int(r)];
    return pick;
  end

  key = string.gsub(key,"pools/","");

  local pick;
  local r = math.mt_random();
  for i,v in ipairs(pool[key].odds) do
    if(r < v) then pick = set[i]; goto DONE; end
  end
  if(pick == nil) then pick = set[1]; end -- you did your odds wrong, fall back to common
  ::DONE::
  return pick; 
end

local all = {"common","armor","weapon","legendary","ammo"};
if(#subpools > 1) then all = utils:a_concat(all,utils:a_slice(subpools,2,#subpools),true); end

local promoted_all = { "common", "ammo" };
local promoted2_all = { "common", "promoted2" };
local promoted3_all = { "common", "promoted3" };
pool = {
  tiered_loot = {
    odds = utils:a_concat({0.8,0.9,0.99,1.0},utils:a_create(1.0,#subpools,true),true), -- impossible to get anythign after legendary
    all = all,
    bad = sets:minus(all,{"legendary"}),
    x = 20,
    picker = picker,
    additional_registration = subpools, -- share the same userstate
  },
  promoted = {
    odds = {0.95,1.0},
    all = promoted_all,
    bad = sets:minus(promoted_all,{"ammo"}),
    x = 10,
    picker = picker_promoted,
  },
  -- additional subpools, should you be inclined to chain them further
  promoted2 = {
    odds = {0.95,1.0}, -- note: promoted2 has a "smaller" set to draw from, it isn't tiered_loot.bad, it is tiered_loot.bad minus the promoted, which is 5% ammo + whatever is pittied
                       -- all this means is, you may want to increase the drop rate, or as you will see in the stats, the probability of promoted2 items is less then ammo
                       -- they can't be the same without doing markov chain probability, an approximation is 0.95^subpool#, for this one 0.95*0.95
    all = promoted2_all,
    bad = sets:minus(promoted2_all,{"promoted2"}),
    x = 10,
    picker = picker_promoted,
  },
  promoted3 = {
    odds = {0.95,1.0},
    all = promoted3_all,
    bad = sets:minus(promoted3_all,{"promoted3"}),
    x = 10,
    picker = picker_promoted,
  },
  is_set = true,
};

return pool;
