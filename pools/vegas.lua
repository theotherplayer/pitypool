
local utils = require("pity/utils");
local pool = {};

-- the payout schedule for x=1, x=2, and x=3
-- if you want to support more, add more
local payouts = {
  {2},
  {1,2},
  {1,2,5},
  {1,2,3,4},
  {1,2,3,4,5},
};
local cost = 0.05;

local annotate = function(options,header,out)
  if(type(options) ~= "table" or type(out) ~= "table") then return nil,"bad parameters"; end
  -- annotating the post states output array with additional data
  -- in this case, some additional columns to deal with payouts

  if(options.groups > #payouts) then return out,nil; end
  local ct = #out;
  if(ct == 0) then return out,nil; end
  local m = utils:to_unique_dict(header,function(i,v) return i; end);
  local offset = #header;

  local extra = {"cost","payout","profit","total_payout","total_profit","total_cost","p_payout"};
  local m2 = utils:to_unique_dict(extra,function(i,v) return i; end);
  for i2,v2 in ipairs(extra) do
    header[offset+i2] = v2;
  end

  local cpayout = 0.0;
  local cprofit = 0.0;
  local ccost = 0.0;
  for i=1,ct do
    local v = out[i];
    local tmp = {};
    -- copy it, since it is a meta version, which can't be increased
    for i2,v2 in ipairs(v) do
      tmp[i2] = v2;
    end
    v = tmp;
    local payout = 0;
    local num_good = v[m["num_good"]];
    if(num_good ~= 0) then
      payout = payouts[options.groups][num_good];
    end

    local total_payout = (payout*cost)*v[m["count"]];
    local total_profit = (payout*cost - cost)*v[m["count"]]; 
    local total_cost = cost*v[m["count"]]; 

    cpayout = cpayout + total_payout;
    cprofit = cprofit + total_profit;
    ccost = ccost + total_cost;

    v[offset+m2["cost"]] = string.format("$%.02f",cost); 
    v[offset+m2["payout"]] = string.format("$%.02f",payout*cost); 
    v[offset+m2["profit"]] = string.format("$%.02f",payout*cost-cost);
    v[offset+m2["total_payout"]] = string.format("$%.02f",total_payout);
    v[offset+m2["total_profit"]] = string.format("$%.02f",total_profit);
    v[offset+m2["total_cost"]] = string.format("$%.02f",total_cost);
    v[offset+m2["p_payout"]] = "";

    out[i] = v;
  end
  -- adding totals
  local a = utils:a_create("",#header);
  a[offset+m2["total_payout"]] = string.format("$%.02f",cpayout);
  a[offset+m2["total_profit"]] = string.format("$%.02f",cprofit);
  a[offset+m2["total_cost"]] = string.format("$%.02f",ccost);
  if(ccost == 0) then
    a[offset+m2["p_payout"]] = string.format("%.05f",0.0);
  else
    a[offset+m2["p_payout"]] = string.format("%.05f",1.0+cprofit/ccost);
  end
  table.insert(out,a);

  return out;
end

local picker = function(pity,key,set,userstate,pity_shown)
  if(set ~= pool.all) then
    -- the picker is called when you pity, with a different set
    -- so just pick anything, since we are only manipulating the all set
    local r = utils:to_int(math.mt_random(1,#set));
    return set[r];
  end 

  local r = math.random(); -- +1?
  local odds = pool.odds;
  for i,row in ipairs(odds) do
    if(r < row) then
      return set[i];
    end
  end 
  return set[#set];
end

pool = {
  groups=3,
  stats = {
    annotate = annotate,
    description=function()
      return string.format("An example slot machine.  The payouts change for different --groups (up to 5). No real slot machine relies on one x, or a fixed x.  But even with this simple model changes in x change the slot machines overall p_payout (the percentage of money that goes in, that is payed back to the customer).");
    end,
  },
  all = {"diamond","bar","spade","heart","cherry"},
  odds = { 0.23, 0.46, 0.69, 0.75  },  -- the important is the last, you have a 2% (difference between 98 and 100, to get a good result
  bad = {"diamond","bar","spade","heart","club"},
  x = 5,
  picker = picker,
  warning = "Don't use this code for real world applications, there are possibly human predictable patterns which this code is not designed to prevent.  It is designed to show how with just -s and a probability table, you can create a variable payout slot machine which as a player 'feels better' (you win more).",
};

return pool;
