
local pool = {};
local utils = require('pity/utils');

local picker_fair = function(pity,key,set,userstate,pity_shown)
  if(set ~= pool.all) then
    local r = math.mt_random(1,#set);
    return set[utils:to_int(r)];
  end

  local r = math.mt_random();
  for i,v in ipairs(pool.odds) do
    if(r < v) then return set[i]; end 
  end
  return set[#set];
end

pool = {
  odds = {0.5}, -- only have to specify |all|-1 odds, as the remainder is the other
  description=function()
    local x = pool.x;
    local extra = string.format("I've shown pity upon you and if you ever throw %d tails in a row, I'll flip it to heads.  Bow down before my glory.",x);
    if(x == -1) then
      extra = "";
    end
    return string.format("You are flipping coins.  A good outcome (heads) vs bad outcomes (tails).  This coin is fair, 50/50 heads to tails.%s",extra);
  end,
  all = {"heads","tails"},
  bad = {"tails"},
  x = 3,
  picker = picker_fair
};

return pool;
