local x = 10;
local quest_drop_rate = 5;
local utils = require('pity/utils');

-- the probability of the rewards from teh "all set"
local pool = {};
local drop_rates = {};
drop_rates[1] = quest_drop_rate/100.0;
drop_rates[2] = drop_rates[1]+0.25;
drop_rates[3] = drop_rates[2]+0.15;
drop_rates[4] = drop_rates[3]+0.15;

local picker = function(pity,key,set,userstate,pity_shown)
  if(set ~= pool.all) then
    -- not the all set, the good set
    local r = utils:to_int(math.random(1,#set));
    return set[r];
  end
  local r = math.random();
  local reward = 5; -- "nothin'"
  for i,v in ipairs(drop_rates) do
    if(r < v) then reward = i; goto DONE; end
  end
  ::DONE::;
  return set[reward];
end

pool = {
  stats = {
    description=string.format("You have a quest, to collect quest_items.  A player kills an NPC and picks at the rewards.  You want the quest_item to be rare ... like %d%% rare (change the picker).  But you've decided that you also want players to progress ... so you place pity on them, and guarentee that every %dth (X) kill will be a quest_item.",quest_drop_rate,x),
    post_analyze=function(stats)
print("stats:",stats);
    end,
  },
  all = {"quest_item","trashA","trashB","trashC","nothin'"},
  bad = {"trashA","trashB","trashC","nothin'"},
  odds = drop_rates,
  x = x,
  picker = picker,
}

return pool;
