
local sets = require("pity/sets");
local utils = require("pity/utils");
local stats = {};

-- NOTE
-- If you were to do any real statistics, on large datasets ... lua, python, perl, are a poor choice
-- for the implementation.  You should be using a compiled, strongly typed, memory efficient datastructure. 
--
-- This means, use a library output by a strongly typed, memory efficent library (often c or c++).
--
-- For python its scipy or just learn go.
-- For lua this is luarocks (a package manager) and c libraries
--
-- You can cheat a bit with magic methods (the __ and setmetatable/getmetatable).  I.e. create something that lua treats as a table,
-- but in actuality isn't ... example it could be using a sqlite table, or amazon lamda.  Just beware of the limitations of "pure" lua.
--
--

local empty_annotations = {};
function stats:append(i,picked,shown_pity,pool,annotations)
  if(self.stats == nil) then self.stats = {raw={},annotations={},mannotations={},rmannotations={},annotations_ct=0}; end
  if(annotations == nil) then annotations = empty_annotations; end
  if(type(annotations) ~= "table") then return nil,"bad annotations"; end

  for k,v in pairs(annotations) do
    if(type(v) ~= "number") then return nil,"bad annotations, value not a number"; end
  end

  for k,v in pairs(annotations) do
    if(self.stats.mannotations[k] == nil) then
      self.stats.annotations_ct = self.stats.annotations_ct+1;
      self.stats.mannotations[k] = self.stats.annotations_ct;
      self.stats.rmannotations[self.stats.annotations_ct] = k;
    end
  end
  -- for later stats arithmatic
  if(shown_pity) then 
    shown_pity = 1;
  else
    shown_pity = 0;
  end 
  local good = sets:minus(pool.all,pool.bad); -- could be cached, but it depends if your pool.all changes dynamically, which it can
  local is_good = sets:contains(good,picked);
  -- stats uses +, which doesn't dynamically cast a bool
  if(is_good) then is_good = 1;
  else is_good = 0;
  end
  table.insert(self.stats.raw,{i,picked,shown_pity,pool,is_good}); -- stored for groups
  table.insert(self.stats.annotations,annotations);
end
function stats:wipe()
  _stats = {};
  return true;
end

function stats:csv(tbl)
  for i,v in ipairs(tbl) do
    print(utils:to_csv( utils:to_strings(v) ) );
  end
end

function stats:pprint(pool,options)
  if(pool == nil or type(pool) ~= "table" or options == nil or type(options) ~= "table") then return nil,"bad parameters"; end

  -- print out conifiguration options
  if(not options.csv) then
    local out = {};
    table.insert(out,{"option","value"});
    for k,v in pairs(options) do
      table.insert(out,utils:map(utils:to_strings({k,v}),function(i,v) return utils:trim(v,25,true); end));
    end
    utils:a_pprint(out);
    io.write("\n");
  end

  if(options.raw) then
    local headers = {"pk","pick","pity_count","pool","is_good"};
    if(options.csv) then
      self:csv(utils:a_concat({headers},utils:map(self.stats.raw,function(i,v) return utils:to_strings(v); end)));
    else
      utils:a_pprint(utils:a_concat({headers},utils:map(self.stats.raw,function(i,v) return utils:to_strings(v); end)));
    end;
    return true;
  end

  if(options.groups ~= nil and (type(options.groups) ~= "number" or options.groups < 1)) then return nil,"bad groups"; end
  if(options.groups == nil) then options.groups = 1; end
  local pk = 1;
  local annotations_ct = self.stats.annotations_ct;

  local grouped = utils:passert(utils:a_group(self.stats.raw,options.groups,function(t,start_index)
    -- t is a table of grouped rows, if groups=3, then it has 3 elements
    -- t is set in the stats:append way above this
    local out = {};
    out[1] = pk; -- a new id for this row
    out[2] = utils:passert(utils:a_create(function(tbl,i)
      return t[i][2];
    end,options.groups,true));
    local mt = getmetatable(out[2]) or {};
    mt.__tostring = function(self)
      -- if printed, we concatinate the values
      return utils:join(" ",utils:to_strings(self)); 
    end
    setmetatable(out[2],mt); 
    out[3] = utils:sum( utils:map(t,function(i,v) return v[3]; end), 1,options.groups ); -- sum the shown_pity, for this group
    out[4] = t[1][4]; -- the pool is shared across all items that are picked, so we can just pick the first
    out[5] = utils:sum( utils:map(t,function(i,v) return v[5]; end), 1, options.groups );
    out[6] = #out[2];
    for i=1,annotations_ct do
      out[6+i] = 0;
      local acc = 0;
      for e=start_index,(start_index+options.groups)-1 do
        local annotation = self.stats.annotations[e];
        if(annotation ~= nil) then
          local v = annotation[self.stats.rmannotations[i]];
          if(v == nil) then v = 0; end
          acc = acc+v; 
        end 
      end 
      out[6+i] = acc;
    end

    pk = pk+1;
    return out;
  end,true));
  if(options.clusters) then
    grouped = utils:passert(utils:a_cluster(grouped,options.clusters,function(t)
      return t[5]; -- the num_good
    end));
    grouped = utils:passert(utils:map( grouped, function(i,v)
      local out = {};
      out[1] = i;
      out[2] = utils:map( v, function(i,v) return table.unpack(v[2]); end ); -- they are grouped, this flattens it
      setmetatable(out[2],{__tostring = function(self) return utils:join(" ",self); end }); -- enable converting to string 
      out[3] = utils:sum( utils:map(v, function(i,v) return v[3]; end ) );
      out[3] = utils:sum( utils:map( v, function(i,v) return v[3]; end ) );
      out[4] = v[1][4];
      out[5] = utils:sum( utils:map( v, function(i,v) return v[5]; end ) );
      out[6] = utils:sum( utils:map(v, function(i,v) return v[6]; end ) );
      for i=1,annotations_ct do
        out[6+i] = utils:sum( utils:map(v, function(i,v) return v[6+i]; end ) );
      end
      return out;
    end));
  end
  local header = {"pk","picked","pity_count","pool","num_good","picks"};
  if(annotations_ct > 0) then
    header = utils:a_concat(header,self.stats.rmannotations);
  end

  -- aggregate
  local stats = {};
  header = utils:a_concat(header,{"count","p_total","p_containing_pity","is_good"},true); -- we are going to manipulate it, so true
  local m = utils:to_unique_dict(header,function(i,v) return i; end);
  local newpk = 0;
  for i,v in ipairs(grouped) do
    local tmp = v[m["picked"]];
    if(options.mergebad) then
      tmp = utils:map(tmp,function(e,v2)
        if(sets:contains(pool.bad,v2)) then return "bad"; end
        return v2;  
      end);
      setmetatable(tmp,{__tostring = function(self) return utils:join(" ",self); end }); -- enable converting to string 
    end
    if(options.unordered) then
      table.sort(tmp,function(a,b) return a<b; end);
    end
    if(options.times) then
      local mt = getmetatable(tmp) or {};
      mt.__tostring = function(self)
        local u = {};
        for k,v in pairs(self) do
          if(u[v] == nil) then u[v] = 0; end
          u[v] = u[v] + 1;
        end
        local out = {};
        for k,v in pairs(u) do
          table.insert(out,string.format("%s.x%d",k,v));
        end
        table.sort(out,function(a,b) return a<b; end);
        return utils:join(" ",out);
      end
      setmetatable(tmp,mt);
    end
    local key = utils:to_strings({tmp})[1]; -- flatten the table
    if(stats[key] == nil) then
      stats[key] = utils:a_create(0,#header);
      stats[key][m["picked"]] = key;
      stats[key][m["pool"]] = v[m["pool"]];
      newpk = newpk+1;
      stats[key][m["pk"]] = newpk;
      for e=1,annotations_ct do
        stats[key][m[self.stats.rmannotations[e]]] = 0;
      end
    end 
    stats[key][m["pity_count"]] = stats[key][m["pity_count"]] + v[m["pity_count"]];
    stats[key][m["count"]] = stats[key][m["count"]] + 1;
    stats[key][m["num_good"]] = v[m["num_good"]];
    stats[key][m["picks"]] = v[m["picks"]];
    if(stats[key][m["num_good"]] > 0) then stats[key][m["is_good"]] = 1; end

    for e=1,annotations_ct do
      local skey = m[self.stats.rmannotations[e]];
      stats[key][skey] = stats[key][skey] + v[skey];
    end
  end
  -- calculate the p_* columns
  local ct = #grouped;
  for k,v in pairs(stats) do
    v[m["p_total"]] = v[m["count"]]/(ct*1.0);
    v[m["p_containing_pity"]] = math.min(1.0,v[m["pity_count"]]/(v[m["count"]]*v[m["picks"]])*1.0);
  end

  local a = utils:a_sort(utils:values( stats ),function(a,b) if(a[m["p_total"]] > b[m["p_total"]]) then return true; end return false; end);
  if(pool.stats ~= nil and type(pool.stats.annotate) == "function") then
    -- call pool specific annotation function (used in vegas for instance)
    local tmp,msg = pool.stats.annotate(options,header,a);
    if(tmp == nil) then return nil,msg; end
    a = tmp; 
  end
  for i,v in ipairs(a) do
    v[1] = i; -- redo the pk, from sorting
  end 
  if(options.csv) then
    self:csv( utils:a_concat({header}, utils:map(a,function(i,v) return utils:to_strings(v); end) ) );
    os.exit(0);
  end
  utils:a_pprint( utils:a_concat({header}, utils:map(a,function(i,v) return utils:to_strings(v); end) ) );

  -- calculate some stats on the raw data
  local ct = #self.stats.raw;
  if(ct == 0) then return; end
  print("");
  local counts = {};
  for i,k in ipairs(pool.all) do
    counts[k] = 0;
  end 
  local good = sets:minus(pool.all,pool.bad);
  local min_bad = 0;
  local max_bad = 0;
  local bad_ct = 0;
  local bad = {};
  for i,v in ipairs(self.stats.raw) do
    if(counts[v[2]] == nil) then counts[v[2]] = 0; end
    counts[v[2]] = counts[v[2]] + 1; 
    if(sets:contains(good,v[2])) then
      max_bad = math.max(max_bad,bad_ct);
      if(min_bad == 0) then min_bad = bad_ct; end
      min_bad = math.min(min_bad,bad_ct);
      table.insert(bad,bad_ct);
      bad_ct = 0;
    else
      bad_ct = bad_ct + 1;
    end
  end
  local ct = #self.stats.raw;
  local header = {"pick","count","p_odds","odds"};
  local out = {};
  for k,v in pairs(counts) do
    table.insert(out,{k,v,v/ct});
  end
  table.sort(out,function(a,b) return a[3]>b[3]; end);
  local odds = pool.odds;
  local modds = utils:to_unique_dict(pool.all,function(i,v)
    if(pool.odds == nil) then return nil; end
    if(i == #pool.all and #pool.all == #pool.odds+1) then
      local s = odds[#pool.all-1];
      if(s == nil) then return "-"; end
      return 1.0-s;
    end
    if(odds[i] == nil) then return "-"; end
    local o = odds[i];
    local s = odds[i-1];
    if(s == nil) then return o; end
   
    -- accumulate the odds for the same key, should it be duplicated
    local ssame = 0.0;
    for e=1,i-1 do
      if(pool.all[e] == v) then
        local p = pool.odds[e];
        local prev = pool.odds[e-1];
        if(prev ~= nil) then p = p-prev; end
        ssame = ssame+p; 
      end
    end
    return o - (s-ssame);
  end);
  out = utils:map(out,function(i,v) return utils:to_strings(utils:a_concat(v,{modds[v[1]] or "-"})); end);
  utils:a_pprint( utils:a_concat({header},out) );
  print("\nmax_bad:",max_bad);
  print("min_bad:",min_bad);
  if(#bad ~= 0) then
    print("avg_bad:",string.format("%.05f",utils:sum(bad)/#bad));
  end

  return true;
end

return stats;
