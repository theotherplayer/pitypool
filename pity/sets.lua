local sets = {};

-- basic implementation of set theory functions used in my pity implementation
-- NOTE: caching layers, systems that use databases like sqlite, will speed these example implementations up

function sets:contains(table1,value)
  if(type(table1) ~= "table" or value == nil) then return nil,"bad parameters"; end
  for i,v in pairs(table1) do
    if(v == value) then return true; end
  end
  return false;
end

function sets:minus(table1,table2)
  if(type(table1) ~= "table" or type(table2) ~= "table") then return nil,"bad parameters"; end

  local out = {};
  for i,v in pairs(table1) do
    local found = false;
    for e,v2 in pairs(table2) do
      if(v == v2) then found = true; goto DONE; end
    end
    ::DONE::
    if(found) then
    else
      table.insert(out,v);
    end
  end
  return out;
end



return sets;
