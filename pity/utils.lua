
local utils = {};
local ParseCSVLine = require('pity/ParseCSVLine');

local mt19937ar = require('pity/external/mt19937ar/mt19937ar');
local mtwist = nil;
_G.math.mt_count = 0;
_G.math.mt_randomseed = function(seed)
  mtwist = mt19937ar.new();
  mtwist:init_genrand(seed);
  return true;
end
_G.math.mt_random = function(x,y)
  -- NOTE: no pure way in lua to determine an int from a float 0.0 vs 0
  -- as a result, this library ONLY outputs floats
  -- you can in the lua c-api.
  if(mtwist == nil) then math.randomseed(tonumber(tostring(os.time()):reverse():sub(1,6))); end
  local p = mtwist:genrand_real2(); -- should return [0,1)
_G.math.mt_count = _G.math.mt_count+1;
  if(x ~= nil and y ~= nil) then
    local diff = y-x;
    if(diff < 0) then return nil,"bad parameters"; end
    diff = diff + 1.0;
    -- no easy way to tell if it is a float vs an int i.e. 0.0 is an int by utils:is_int (which the c api can do, so return floats in all cases) 
    -- this means you can NOT expect integers out
    return x + diff*p;
  elseif(x ~= nil) then
    if(x < 0) then return nil,"bad parameters"; end
    return x*p;
  else
    return p;
  end;
end


function utils:passert(tmp,msg,...)
  if(tmp == nil) then
    local trace = debug.traceback(2);
    assert(false,string.format("msg=%s, trace=%s",msg,trace));
  end
  return tmp,msg,...;
end
function utils:values(t)
  if(type(t) ~= "table") then return nil,"bad parameters"; end
  local out = {};
  for k,v in pairs(t) do
    table.insert(out,v);  
  end 
  return out;
end
function utils:trim(s,n,eli)
  if(type(s) == "string" and type(n) == "number" and n >= 0) then
    if(string.len(s) > n) then
      if(n == 0) then return ""; end
      if(n >= 3 and eli) then
        return string.sub(s,1,n-3) .."...";
      end
      return string.sub(s,1,n); 
    end
    return s;
  end
  return nil,"bad parameters";
end
function utils:keys(t)
  local out = {};
  for k,v in pairs(t) do
    table.insert(out,k);
  end
  return out;
end

function utils:args(t)
  -- simple command line arg processing
  if(type(t) ~= "table") then return nil,"bad parameters"; end
  for k,v in pairs(t) do
    if(type(v) ~= "table" and #v >= 2) then return nil,"bad parameters"; end
    local l = string.len(k);
    if(l == 0) then
      return nil,"bad parameters";
    end
  end
  local out = {};
  -- set defaults
  for k,v in pairs(t) do
    if(v[2] ~= nil) then out[k] = v[2]; end
  end
  local previous = nil;
  local expected = nil;

  for i,key in ipairs(arg) do
    if(previous ~= nil) then
      if(expected == "b") then
        return nil,"bad parameters";
      elseif(expected == "d") then
        local n = tonumber(key);
        if(n == nil) then return nil,string.format("can't process %s not a number",previous); end
        out[previous] = n;
      elseif(expected == "csv") then
        local a,msg = utils:from_csv(key);
        if(a == nil) then return nil,string.format("can't process %s, not csv",previous); end
        for i,v in ipairs(a) do
          local n = tonumber(v);
          if(n ~= nil) then a[i] = n; end
        end
        out[previous] = a; 
      else
        out[previous] = key;
      end
      previous = nil;
      expected = nil;
    else
      if(not string.match(key,"^[-][-]?[^ ]+$")) then return nil,string.format("unhandled command line arg %s",key); end
      key = string.match(key,"^[-][-]?([^ ]+)$");
      if(t[key]) then
        local ty = t[key][1];
        if(ty == "b") then
          out[key] = true;
        else
          previous = key;
          expected = ty;
        end
      else
        return nil,string.format("unhandled arg %s",key);
      end
    end  
  end
  if(previous ~= nil) then
    return nil,"unbalanced command line paramters";
  end
  return out;
end

function utils:safe_incr(tbl,key,incr)
  if(type(tbl) ~= "table" or not (type(key) == "string" or type(key) == "number")) then return nil,"bad parameters"; end
  if(incr ~= nil and type(incr) ~= "number") then return nil,"bad parameters"; end
  if(incr == nil) then incr = 1; end
  if(tbl[key] == nil) then tbl[key] = 0; end
  tbl[key] = tbl[key]+incr;
  return true;
end

function utils:rpad(s,l,ch)
  if(type(s) ~= "string" or type(l) ~= "number") then return nil,"bad parameters"; end
  ch = ch or " ";
  if(string.len(s) > l) then return string.sub(s,1,l); end
  while(string.len(s) < l) do
    s = s .. ch;
  end
  return s;
end
function utils:lpad(s,l,ch)
  if(type(s) ~= "string" or type(l) ~= "number") then return nil,"bad parameters"; end
  ch = ch or " ";
  if(string.len(s) > l) then return string.sub(s,1,l); end
  while(string.len(s) < l) do
    s = ch .. s;
  end
  return s;
end

function utils:join(delim,t) -- yep, perl order... suckas
  -- joins arrays, not dictionaries ... that support is beyond the scope of this toy project ;)
  if(type(t) ~= "table") then return nil,"bad parameters"; end
  if(delim == nil) then delim = ""; end
  local out = "";
  local ct = #t;
  for i=1,ct do
    local v = t[i];
    local s = utils:to_strings({v})[1];
    out = out .. s;
    if(i < ct) then out = out .. delim; end
  end
  return out;
end

function utils:a_sort(t,f) 
  -- lua table.sort 
  if(type(t) ~= "table" or type(f) ~= "function") then return nil,"bad parameters"; end
  table.sort(t,f);
  return t;
end
function utils:a_slice(tbl,s,e)
  -- didn't bother with static/magic for this project
  if(type(tbl) ~= "table" or type(s) ~= "number" or type(e) ~= "number" or s < 1 or e < 1 or e < s) then return nil,"bad parameters"; end
  return utils:a_create(function(tbl2,idx) return tbl[s+idx-1]; end,e-s+1,true);
end

function utils:a_concat(...)
  -- returns a table that acts like a concatination of all the tables passed ot this function
  -- it will calculate teh size # of each table, but will not examine the rows (should you pass in a table that for instance hits a database)
  local args = {...};
  local counts = {};
  local ct = 0;
  local static = false;
  for i,v in ipairs(args) do
    if(i == #args and type(v) == "boolean") then
      static = v;
    else
      if(type(v) ~= "table") then return nil,"bad parameters"; end
      counts[i] = #v;
      ct = ct + counts[i];
    end
  end
  if(static) then
    local out = {};
    for i,v in ipairs(args) do
      if(i == #args) then goto DONE; end
      for e,v2 in ipairs(v) do
        table.insert(out,v2);
      end
    end
    ::DONE::
    return out;
  end

  local out = {};
  local mt = {};
  mt = {
    __len = function(t)
      return ct;
    end,
    __index = function(t,i)
      if(utils:is_int(i)) then
        if(i > ct) then return nil; end
        local s = 0;
        local sum = 0;
        for tbl_idx,v in ipairs(counts) do
          if(i <= (s+v)) then
            return args[tbl_idx][i-s];
          end
          s = s+v;
        end
        return nil;
      else
        return rawget(t,i);
      end
    end,
    __ipairs = function(self)
      local function iter(tbl,i)
        i = i+1;
        if(i > ct) then return nil; end
        local v = mt.__index(self,i);
        if(v ~= nil) then return i,v; end
      end
      return iter, self, 0
   end,
  };
  setmetatable(out,mt); 
  return out; 
end

function utils:a_each(tbl,f)
  -- copy, since only used once and for stats, so don't care about performance/memory
  if(type(tbl) ~= "table" or type(f) ~= "function") then return nil,"bad parameters"; end
  local out = {};
  for i,row in ipairs(tbl) do
    out[i] = f(i,row);
  end
  return out;
end
function utils:a_cluster(tbl,clusters,f)
  --ex: a_clusters({2,1,2,4,1,2...},5,function(v) return v; end); 
  -- returns { {2,1,2},{4,1},... }, each "cluster" is >= clusters, where the accumulation is the return value from f
  if(type(tbl) ~= "table" or type(clusters) ~= "number" or clusters < 1 or type(f) ~= "function") then return nil,"bad parameters"; end
  local out = {};
  local tmp = {};
  local c = 0;
  for i,v in ipairs(tbl) do
    table.insert(tmp,v);
    local v = f(v);
    if(type(v) ~= "number" or v < 0) then return nil,"bad a_cluster func"; end
    c = c+v;
    if(c >= clusters) then table.insert(out,tmp); tmp = {}; c = 0; end
  end
  if(c >= clusters) then table.insert(out,tmp); end
  return out;
end

function utils:a_group(tbl,groups,mergefunc)
  if(type(tbl) ~= "table" or type(groups) ~= "number" or type(mergefunc) ~= "function" or groups < 1) then return nil,"bad parameters"; end

  local out = {};
  local tmp = {};
  local count = 1;
  for i,v in ipairs(tbl) do
    tmp[count] = v;
    count = count+1;
    if(count > groups) then
      local m,msg = mergefunc(tmp,i-groups+1);
      if(m == nil) then return nil,msg; end
      table.insert(out,m);
      tmp = {};
      count = 1;
    end 
  end
  return out;
end

function utils:a_create(value,num,static)
  if(value == nil or type(num) ~= "number") then return nil,"bad parameters"; end
  -- don't use nil placeholders, use false ... lua doesn't like setting values in dictionaries to nil, it removes the keys
  -- so if your value is a function and returns nil, that is a failure

  if(num <= 0 and type(value) ~= "function" and not static) then return nil,"bad parameters, num<=0 requires a function and static"; end

  if(static) then
    local out = {};
    if(num <= 0 and type(value) == "function") then
      for k,v in value do
        if(v == nil) then table.insert(out,k);  -- iterator with singleton, like string.gmatch
        else out[k] = v; end -- iterator with tuple
      end
      return out;
    end
  
    for i=1,num do
      local v = value;
      local msg = nil;
      if(type(v) == "function") then v,msg = v(out,i); end
      if(v == nil) then return nil,msg or "nil found in a_create value function"; end
      table.insert(out,v);
    end
    return out; 
  end


  local out = {};
  local mt = {
    __len = function() return num; end,
    __index = function(t,i)
      if(type(i) == "number") then
        if(type(value) == "function") then return value(t,i); end
        if(i > num) then return nil; end
        return value;
      end
      return rawget(t,i);
    end,
    __pairs = function()
      -- does not export any keys that aren't between 1,...,num
      local ct = num;
      local function iter(tbl,i)
      while(true) do
          i = i+1;
          if(i > num) then return nil; end
          local v = value;
          if(type(v) == "function") then v = value(t,i); end
          return i,v;
        end
      end
      return iter, self, 0
    end,
  };
  setmetatable(out,mt);

  
  return out;
end

function utils:bits(num)
  -- doesn't support floats
  if(type(num) ~= "number") then
    return nil,"bad parameters";
  end
  local ct = 0;
  while(num > 0) do
    ct = ct+1;
    num = num >> 1;
  end
  return ct;
end

function utils:sum(tbl,s,e)
  if(type(tbl) == "table" and s == nil and e == nil) then s = 1; e = #tbl; end
  if(type(tbl) ~= "table" or type(s) ~= "number" or type(e) ~= "number") then return nil,"bad parameters"; end
  if(s < 1 or e < 1) then return nil,"out of range"; end
  local out = 0;
  for i=s,e do
    out = out+tbl[i];
  end
  return out;
end

function utils:a_pprint(tbl)
  if(type(tbl) ~= "table") then return nil,"bad parameters"; end
  -- every value must be a string, every element is a table of same length
  local ct = #tbl;
  if(ct == 0) then return; end

  local padding = 2;

  local cols_found = nil;
  local widths = utils:passert(utils:a_create(function(x,i)
    local max = 0;
    for idx,row in ipairs(tbl) do
      local v = row[i];
      if(type(v) == "string") then
        local tmp = math.max(string.len(v));
        if(max == 0) then max = tmp; end
        max = math.max(max,tmp); 
      else
        return nil,string.format("non-string value: %s row=%d,col=%d",type(v),idx,i);
      end
    end
    if(cols_found == nil) then cols_found = #tbl; end
    if(cols_found ~= #tbl) then return nil,"mismatched columns across the table"; end
    return max+padding;
  end,#tbl[1],true));

  for r,row in ipairs(tbl) do
    for c,col in ipairs(row) do
      local w = widths[c];
      io.write(utils:rpad(col,w));
    end
    io.write("\n");
  end 
  return true;
end

function utils:to_unique_dict(tbl,v)
  if(type(tbl) ~= "table") then return nil,"bad parameters"; end
  -- every value in the array becomes a key, with teh value v
  local out = {};
  for i,v2 in ipairs(tbl) do
    local tmp = v;
    if(type(tmp) == "function") then tmp = tmp(i,v2); end
    out[v2] = tmp;
  end
  return out;
end
function utils:map(tbl,cbk)
  if(type(tbl) ~= "table" or type(cbk) ~= "function") then return nil,"bad parameters"; end
  -- simple map, inline, O(N),... all the bad stuffs
  local out = {};
  for i,v in ipairs(tbl) do
    out[i] = cbk(i,v);
  end
  return out;
end

function utils:to_csv(t)
  -- returns a string, csv encoded
  -- values should be strings before calling
  if(type(t) ~= "table") then return nil,"bad parameters"; end
  local s = "";
  for i,p in ipairs(t) do
    s = s .. "," .. escaped(p);
  end
  return string.sub(s,2);
end
function utils:from_csv(t)
  local a = ParseCSVLine(t); 
  if(#a == 0) then return nil,"bad parameters"; end
  return a;
end

function utils:is_int(s)
  -- returns true if a number is an int
  if(type(s) == "number") then
    if(s == utils:to_int(s)) then return true; end
  end
  return false;
end
function utils:to_int(s)
  if(type(s) ~= "number") then return nil; end
  local sign = 1;
  if(s < 0) then sign = -1; end
  return math.floor(math.abs(s)) * sign;
end

function utils:extend(d,d2,overwrite)
  if(type(d) ~= "table" or type(d2) ~= "table" or not (overwrite == nil or type(overwrite) == "boolean")) then
    return nil,"bad parameters";
  end
  local out = {};
  for k,v in pairs(d) do
    out[k] = v;
  end
  for k,v in pairs(d2) do
    if(out[k] == nil or overwrite) then out[k] = v; end
  end
  return out;
end

function utils:to_strings(t,ofmts) -- the formatting is specific to this app, string, ints, and 5 decimal floats
  if(type(t) ~= "table") then return nil,"bad parameters"; end
  local fmts = {d="%d",f="%.05f",s="%s"};
  if(type(ofmts) == "table") then
    fmts = utils:extend(ofmts,fmts);
  end

  local tbl = utils:map(t,function(i,v)
   if(type(v) == "string") then
     return string.format(fmts.s,v);
   elseif(utils:is_int(v)) then
     return string.format(fmts.d,v);
   elseif(type(v) == "number") then
     return string.format(fmts.f,v);
   else
     return string.format("%s",v);
   end
  end);
  return tbl;
end

-- HELPER
function escaped(s)
  if(string.find(s,'[,\n"]')) then
    s = '"'..string.gsub(s,'"','""')..'"';
  end
  return s;
end



return utils;
