

local utils = require('pity/utils');
local pools = {};

-- class acts as a singleton, and manages the pools
-- pools are dictionaries (tables in lua) with:
-- .all: set set of possible outcomes
-- .bad: the set of bad outcomes
-- .x: the max number of bad occurrences in a row (or a function, which returns the max, so you can change it as you run the code)
-- .picker: a function that picks from .all, what it returns will be compared to bad and changed by the pity algorithm

local _registered = {};
function pools:register(key)
  local success,pool = pcall(require,key);
  if(not success) then return nil,string.format("failed to find pool key=%s",key); end

  local parts,msg = utils:a_create(string.gmatch(key,"([^/]+)"),0,true);
  local subpath = utils:join("/",utils:a_slice(parts,1,#parts-1)) or "";
  if(subpath ~= "") then subpath = subpath .."/"; end

  local dpool = {[key]=pool};
  if(pool.is_set) then
    for k,v in pairs(pool) do
      if(k ~= "is_set") then
        dpool[string.format("%s%s",subpath,k)] = v;
      end
    end
  end
  if(dpool[key] == nil) then return nil,string.format("key not found in pool key=%s",key); end

  for k,pool in pairs(dpool) do 
    if(type(pool) ~= "table" or 
      type(pool.all) ~= "table" or
      type(pool.picker) ~= "function" or
      not (type(pool.x) ~= "number" or type(pool.x) ~= "function")) then
      return nil,string.format("bad pool key=%s",k);
    end
    local mt = getmetatable(pool) or {};
    mt.__tostring = function() return k; end
    setmetatable(pool,mt);
    _registered[k] = pool
  end
  return true;
end

function pools:get(key)
  if(_registered[key] == nil) then return nil,string.format("not registered, key=%s",key); end
  return _registered[key];
end

return pools;

