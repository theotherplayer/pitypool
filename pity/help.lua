local out = "";

out = [[
ppool.lua

1. Interface to pity.lua
2. Aggregates (1.) into human readable tables to aid in understanding

arguments
--key <pity/pool/*>	: A preset set of --pool.all, --pool.bad and any other set of options.  Including perhaps a custom picker.
-x #			: pity. No more than X bad results in a row, or you force a good.
--pool.all "csv"	: The set of outcomes
--pool.bad "csv"	: The bad outcomes, a subset of pool.all
--num_picks #		: The number of picks from your pool
--odds "csv"		: The default picker uses these to pick from pool.all.
--groups #		: Groups picks into sets, default is 1
--clusters #		: Clusters groups in order until the minimum of # "good" outcomes are in the set
--unordered		: The picked items in a group have no order.  --groups 3, heads tails tails, is the same as tails tails heads
--mergebad		: All bad outcomes are replaced with "bad"
--times			: Picks are further grouped together.  heads heads tails becomes heads.x2, tails.x1.  Useful for large groups.
--csv			: Output the main table as CSV
--raw			: Output the picks only
--seed #		: Changes to a fixed seed, instead of one based on the system time.  NOTE: sorting of stats doesn't use a seed, so equal probabilities may be on different rows.

Provided arguments overwrite the defaults set via --key.  Example you could keep everything and just change the odds.

See the pity/examples/ directory for more examples.
]];


return out;
