
local pity = {};

local pools = require("pity/pools");
local sets = require("pity/sets");
local utils = require("pity/utils");

local _registered = {};

-- Tieing a pool to a location to store the userstate

function pity:register(userstate,key)
  if(type(userstate) ~= "table" or type(key) ~= "string") then return nil,"bad parameters"; end
  local pool = utils:passert( pools:get(key) );
  if(pool.additional_registration ~= nil and type(pool.additional_registration) ~= "table") then
    return nil,"bad pool additional_registration";
  end

  local parts,msg = utils:a_create(string.gmatch(key,"([^/]+)"),0,true);
  local subpath = utils:join("/",utils:a_slice(parts,1,#parts-1)) or "";
  if(subpath ~= "") then subpath = subpath .."/"; end

  _registered[key] = {
    key = key,
    userstate = userstate,
    pool = pool,
  };
  -- its possible a pool makes use of other pools with the same userstate
  -- used in the hearthstone example
  if(pool.additional_registration ~= nil) then
    for i,v in ipairs(pool.additional_registration) do
      self:register(userstate,string.format("%s%s",subpath,v));
    end
  end

  return true;
end

-- 
-- Picks an outcome from a pool and enforces the Pity for that pool.
--
-- To implement this algorithm you will need:
-- all - the possible outcomes
-- bad - the bad outcomes
-- x - the number of bad outcomes in a row, before you flip it to a good outcome
-- good - the good outcomes, set:minus(all,bad)
-- userstate - a dictionary that can store the counter of your current x
--
--
-- This is not optimized for speed (for instance I don't care the good).  And all but the good are stored in an object _registered (using pity:registered).
--
-- Returns: an outcome from the pool.all, and whether pity was shown 

function pity:pick(key)
  if(key == nil) then return nil,"bad parameters"; end
  if(_registered[key] == nil) then return nil,string.format("pity:register for this key not called, key=%s",key); end

  local config = _registered[key];
  local bad_key = string.format("%s.num_bad",key);
  local pity_key = string.format("%s.num_pity",key);
  if(config.userstate[pity_key] == nil) then config.userstate[pity_key] = 0; end
  if(config.userstate[bad_key] == nil) then config.userstate[bad_key] = 0; end

  local x = config.pool.x;
  if(type(x) == "function") then x = x(key,config.userstate[pity_key],config.userstate[bad_key]); end
  if(type(x) ~= "number") then return nil,"bad x"; end

  local picked,annotations = config.pool.picker(pity,key,config.pool.all,config.userstate,false);
  if(picked == nil) then return nil,"picker returned nil (for a pick from pool.all)"; end
  if(sets:contains(config.pool.bad,picked)) then
    utils:safe_incr(config.userstate,key);
    utils:safe_incr(config.userstate,bad_key);
  else
    config.userstate[key] = 0;
  end

  local pity_shown = false;

  if(config.userstate[key] > x and x > 0) then
    pity_shown = true;
    utils:safe_incr(config.userstate,pity_key);
    local good = sets:minus(config.pool.all,config.pool.bad);
    picked = config.pool.picker(pity,key,good,config.userstate,true); -- repick i.e. show pity
    if(picked == nil) then return nil,string.format("picker returned nil (for a good pick)"); end
    config.userstate[key] = 0;
  end
  return picked,pity_shown,annotations;  
end

return pity;
