Lets look at a few coins examples.



lua ppool.lua --key pools/coins.lua --x -1 --num_picks 100000 --seed 0
count:	100000
option     value        
seed       0            
help       false        
key        pools/coins  
num_picks  100000       
raw        false        
csv        false        
x          -1           
odds       0.50000      

pk  picked  pity_count  pool         num_good  picks  count  p_total  p_containing_pity  is_good  
1   heads   0           pools/coins  1         1      50103  0.50103  0                  1        
2   tails   0           pools/coins  0         1      49897  0.49897  0                  0        

pick   count  p_odds   odds     
heads  50103  0.50103  0.50000  
tails  49897  0.49897  0.50000  

max_bad:	17
min_bad:	1
avg_bad:	0.99589


This is 100k flips of a fair coin.  Feel free to metl your computer with higher num_picks.



lua ppool.lua --key pools/coins.lua --x 5 --num_picks 100000 --seed 0
count:	100751
option     value        
raw        false        
num_picks  100000       
help       false        
key        pools/coins  
csv        false        
seed       0            
x          5            
odds       0.50000      

pk  picked  pity_count  pool         num_good  picks  count  p_total  p_containing_pity  is_good  
1   heads   751         pools/coins  1         1      50827  0.50827  0.01478            1        
2   tails   0           pools/coins  0         1      49173  0.49173  0                  0        

pick   count  p_odds   odds     
heads  50827  0.50827  0.50000  
tails  49173  0.49173  0.50000  

max_bad:	5
min_bad:	0
avg_bad:	0.96744



The same odds of pulling a heads/tails (50/50) but you prevent any streak greater then 5 bad (tails) in a row.  You can see that the odds improve almost 0.01% for heads! but the player will never see a streak of 17 tails in a row.

This, in a nutshell, is all the algorithm does.  It allows the developer to market "And you can never fail 6 times in a row!".


lua ppool.lua --key pools/coins.lua --x 1 --num_picks 100000 --seed 0
count:	116604
option     value        
num_picks  100000       
help       false        
seed       0            
csv        false        
raw        false        
odds       0.50000      
key        pools/coins  
x          1            

pk  picked  pity_count  pool         num_good  picks  count  p_total  p_containing_pity  is_good  
1   heads   16604       pools/coins  1         1      66680  0.66680  0.24901            1        
2   tails   0           pools/coins  0         1      33320  0.33320  0                  0        

pick   count  p_odds   odds     
heads  66680  0.66680  0.50000  
tails  33320  0.33320  0.50000  

max_bad:	1
min_bad:	1
avg_bad:	0.49970

We've made it so after 1 tail, we guarenete a head.  This means 66% heads.  If you want to see it more clearly, lets group by 2.
