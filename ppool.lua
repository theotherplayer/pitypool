
local sets = require("pity/sets");
local utils = require("pity/utils");
local pity = require("pity");
local pools = require("pity/pools");
local stats = require("pity/stats");

local userstate = {}; -- a dictionary, where we store the various counters used by the Pity pool
                      -- this could be a database
                      -- in lua it is a table which supports [] (for setting and getting) and # (for length) 

local args = {
  key={"s",nil},
  num_picks={"d",10000},
  groups={"d",nil}, -- group picks (ex: --groups 3, --num_picks 9 would mean 3 groups of 3)
  clusters={"d",nil}, -- groups rows (or groups) into >= X num_goods (if you don't group, clusters will equal this number)
  x={"d",nil},
  raw={"b",false}, -- prints out the raw picks
  csv={"b",false}, -- outputs the picks as csv, so you can do your own calculations externally (like in scipy or matlab)
  mergebad={"b",nil}, -- bad results become the word bad
  unordered={"b",nil}, -- sorts the picks... bad good and good bad are put in teh same stats buckets
  times={"b",nil}, -- bad bad bad becomes bad.x3 (shrinks output labels)
  odds={"csv",nil},
  seed={"d",-1},
  help={"b",false},
};

local options = utils:passert(utils:args(args));
if(options.seed == -1) then
  -- utils adds mt_random and mt_randomseed.
  -- these are predictable random number generators (with a seed), and cross platform
  -- the drawback is you need to call int vs float libraries separately (vs the math.random built into lua)
  math.mt_randomseed(tonumber(tostring(os.time()):reverse():sub(1,6)));
else
  math.mt_randomseed(options.seed);
end

if(options.help) then
  print(require("pity/help"));
  os.exit(0);
end
if(options.key == nil) then
  print("ppool.lua --key <pool code> [--x 10] [--csv] [--num_picks 200] [--groups 2] [--clusters 2] [--mergebad] [--unordered] [--times] [--odds \"0.2,0.3\"] [--help]");
  os.exit(1);
else
  if(string.match(options.key,"^(.*).lua$")) then
    options.key = string.match(options.key,"^(.*).lua$");
  end
end

utils:passert( pools:register(options.key) );
utils:passert( pity:register(userstate,options.key) ); -- tie a pool to a particular userstate

local pool = utils:passert(pools:get(options.key));

-- propigate the defaults from the pool to options, and options to the pool
utils:a_each( utils:keys(args), function(i,key) if(options[key] == nil) then options[key] = pool[key]; end end );
utils:a_each( utils:keys(options), function(i,key) pool[key] = options[key]; end ); -- copy options into the pool, overwriting
-- make options.odds printable
if(options.odds) then setmetatable(options.odds,{__tostring = function(self) return utils:join(",",utils:map(self,function(i,v) return string.format("%.05f",v); end) ); end }); end

for i=1,options.num_picks do
  local picked,shown_pity,annotations = utils:passert(pity:pick(options.key)); -- this is all that is important, pools:register and pity:reister, and many calls to pity:pick, the rest is for printing stats
  stats:append(i,picked,shown_pity,pool,annotations);
end

utils:passert(stats:pprint(pools:get(options.key),options));

