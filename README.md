# Pity Pool. 

## What does a pitty pool do?
It converts a bad outcome into a good outcome when X bad outcomes occur in a row.  At its core, it is "bad RNG protection".

## Uhhmmmm...

It solves problems like:
1. I want bad things occur no more fequently then X times in a row.
2. I want to guarentee that it takes fewer then X*number_of_drops_required kills to complete a quest.
3. Happy players pay more, so I give them results they perceive as wins ... all the while raking in the money (gambling).
4. I want to have marketing text "A Legendary in every 5th pack!", all the while hiding frequency and drop information on a server somewhere never exposing it unless government laws change ... aka To make money off of lootboxes.  Just don't ask me if the probability of each legendary is fair ... because ... it likely isn't.
etc


# The algorithm.

1. Pool.all the set of all outcomes
2. Pool.bad the set of bad outcomes
2. Pool.good the set of good outcomes (for this code good = sets:minus(all,bad) )
3. picker function to pick an outcome from a Pool. 
4. userstate - place to store the current streak of failures
5. x - the number of bad outcomes in a row, before you force a good

pick = picker(pool.all)

Is pick a member of set Pool.bad? userstate.counter += 1;
Is userstate.counter >= x:  then pick = picker(pool.good);  userstate.counter = 0; *pitied them*

return the pick

The algorithm is in pity.lua.  The rest of the code shows examples of more complicated pickers and print out tables.


## How to use this code:
1. Install lua
2. Download the code
3. Run lua ppool.lua

## But I don't want to look at the code?
Look at examples/

# Why lua?
Common language for video game programmers.  For statistics lua is not ideal.  Lua is often viewed as a front end to higher performance languages (like c).  This project a pure lua project and the statistics part exists with the lower performance expectations lua provides.  It wasn't written for speed or low memory usage.  But it is pretty easy to bind higher performance libraries to lua (ex: luarocks and c/c++).


# Why did you do this?
;tldr; because, I was bored.

I was creating a suggestion for a 3rd party and it occured that to explain it ... either they already knew, and it wasn't needed (likely) ... or the explaination would take up a large amount of text.  I decided to separate the explaination, broaden who could potentially consume it, and simply reference it in the suggestion.

