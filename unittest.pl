#!/usr/bin/env perl -w
use strict;

use IO::File;

# run all of the lines which start with "lua ..." in examples
# if they don't return an exit status of success, then an error happened

my @matches = ( split(/\n/,`grep -h "^lua ppool.lua" examples/*`) );
if(scalar(@matches) == 0) { die "cant parse examples"; }

for my $match (@matches) {
  if($match =~ /num_picks/) {
    $match =~ s/num_picks (\d+)/num_picks 1000/g;
  } else { 
    $match .= " --num_picks 1000";
  }
  my $output = `$match 2>&1`;
  my $status = $? >> 8;
  if($status != 0) { die $match . "\n\n" . $output; }
}

=pod

Why perl?  Because perl.
